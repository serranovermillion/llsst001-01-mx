package com.bbva.llss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Consulta cuenta bancaria
 *
 */
public class LLSST00101MXTransaction extends AbstractLLSST00101MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(LLSST00101MXTransaction.class);

	/**
	 * The execute method...
	 */
	@Override
	public void execute() {
		// TODO - Implementation of business logic
	}

}
