package com.bbva.llss;

import com.bbva.elara.transaction.AbstractTransaction;

/**
 * In this class, the input and output data is defined automatically through the setters and getters.
 */
public abstract class AbstractLLSST00101MXTransaction extends AbstractTransaction {

	public AbstractLLSST00101MXTransaction(){
	}


	/**
	 * Return value for input parameter nuCuenta
	 */
	protected String getNucuenta(){
		return (String)this.getParameter("nuCuenta");
	}

	/**
	 * Return value for input parameter cdDivisa
	 */
	protected String getCddivisa(){
		return (String)this.getParameter("cdDivisa");
	}

	/**
	 * Return value for input parameter cdTCuenta
	 */
	protected String getCdtcuenta(){
		return (String)this.getParameter("cdTCuenta");
	}

	/**
	 * Set value for String output parameter importe
	 */
	protected void setImporte(final String field){
		this.addParameter("importe", field);
	}
}
